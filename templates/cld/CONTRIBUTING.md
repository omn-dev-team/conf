# Einmalige Initialisierung
Schau dazu in das open CONTRIBUTING, letzter Punkt.


# Ich kann den closed Bereich nicht forken! Wie kann ich etwas pushen?
Im closed Bereich arbeitest du direkt auf dem UPSTREAM Repo. Das heißt du erstellst keinen Fork, sondern einen eigenen Branch auf dem UPSTREAM Repo.
Dazu erstellst du auf deinem lokalen PC einen branch (Ast oder Verzweigung), welcher dann auf das UPSTREAM Projekt gepusht wird und dort in den master branch über einen Merge Request zusammengeführt wird.

## Auf eigenen Branch wechseln
Hier wird beschrieben, wie du auf deinen eigenen Branch wechseln kannst.

1. Schau dir an auf welchem branch du dich befindest.

	```
	$ git branch
	* master
	  dein_gitlab_benutzername
	```
1. Sollte dein_gitlab_benutzername nicht in der Liste aufscheinen, erstellst du mit folgendem Befehel dir einen Branch.

	```
	$ git branch dein_gitlab_benutzername
	```
1. Wechsel auf deinen Branch mit folgendem Befehl

	```
	$ git checkout dein_gitlab_benutzername
	```
1. Überprüfe ob du auf deinem Branch bist
	
	```
	$ git branch
	  master
	* dein_gitlab_benutzername
	```

## Updates vom UPSTREAM Projekt holen
Hier wird beschrieben, wie du neue Inhalte vom UPSTREAM Projekt erhältst.

1. Wechsel in den cld Ordner

	```
	$ cd ./cld
	```
1. Führe den Schritt "Auf eigenen Branch wechseln" aus.
1. Führe den Schritt "Updates vom UPSTREAM Projekt holen" aus dem opn CONTRIBUTING aus.

## Inhalte erstellen
Hier wird beschrieben, welche Schritte notwendig sind damit du Inhalte erstellen kannst. Führe diese Schritte jedes mal in dieser Reinfolge aus.

1. Wechsel in den cld Ordner

	```
	$ cd ./cld
	```
1. Führe den Schritt "Auf eigenen Branch wechseln" aus.
1. Führe den Schritt "Inhalte erstellen" aus dem opn CONTRIBUTING aus.

## Commit erstellen
Erstelle immer zuerst die Commits im closed Bereich und anschließend im open Bereich.

1. Wechsel in den cld Ordner

	```
	$ cd ./cld
	```
1. Führe den Schritt "Auf eigenen Branch wechseln" aus.
1. Führe den Schritt "Commit erstellen" aus dem opn CONTRIBUTING aus.

## Commits Pushen
Hier wird beschrieben wie du deine Commits (können auch mehrere sein) an den GitLab Server vom OMN schickst und einen Merge Request erstellst.

1. Wechsel in den cld Ordner

	```
	$ cd ./cld
	```
1. Führe den Schritt "Auf eigenen Branch wechseln" aus.
1. Führe den Schritt "Updates vom UPSTREAM Projekt holen" aus dem opn CONTRIBUTING aus.
1. Schau dir das log an. Im Gegensatz zum open Bereich befindet sich dein Branch auf dem UPSTREAM Server. 

	```
	$ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> dein_gitlab_benutzername)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890 (upstream/master)
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 (upstream/dein_gitlab_benutzername)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
		
	```

1. Push deinene Commits auf deinen branch auf dem GitLab Server vom OMN.

	```
	$ git push origin dein_gitlab_benutzername
	```
	Das log sieht nun so aus:
	
	```
    $ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> dein_gitlab_benutzername,upstream/dein_gitlab_benutzername)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890 (upstream/master)
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
		
	```
1. Geh auf die closed GitLab Seite der Lehrveranstaltung und erstelle einen Merge-Request.
1. Nachdem dein Merge-Request überprüft wurde, werden deine Commits in den master branch des UPSTREAM Projekts übernommen.
1. Führe den Punkt "Update vom UPSTREAM Projekt holen" aus, damit du am aktuellen Stand bist.
1. Wenn du dir nun wieder das Log ansiehst, wirst du feststellen, dass auch der master branch deine Commits übernommen hat.

    ```
    $ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> dein_gitlab_benutzername,upstream/dein_gitlab_benutzername, upstream/dein_gitlab_benutzername)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
	
	```
