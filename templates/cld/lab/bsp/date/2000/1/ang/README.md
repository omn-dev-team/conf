# Laborübungen
In dem Ordner Labor Beispiele werden Laborübungen abgelegt. Dazu wird im Ordner date die Jahreszahl eingegeben und eine Ordnerebene draunter die Übungsnummer. In jedem Ordner für eine Übung gibt es einen Ordner ang, wo sich die Originalen Angaben ohne Veränderung befinden.

## Lösungen
Jeder user hat dann innerhalb der Übungsnummer seinen eigenen Ordner, wo sich seine Lösung für diese Übung befindet.

## Report
In dem Unterordner rep kann sehr leicht mittels des OMN Templates ein Laborprotokoll erstellt werden.
