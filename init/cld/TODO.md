# Aufgaben im closed Bereich
Die hier beschriebenen Aufgaben können nur im closed Bereich ausgeführt werden.

## Issues bearbeiten
Gleiche Aufgaben wie im open Bereich, jedoch im closed Bereich.

## Einsicht bei Multiple Choice
Multiple Choice Fragen dürfen bei der Einsicht nicht eingescannt werden. Merke dir so gut du kannst die Fragen und schreib sie auf. Stelle das .pdf in /cld/exm/mpc/date/JahrMonatTag/JahrMonatTag.pdf hinein.

## Multiple Choice Prüfungen
Tippe eine Frage zu einer Multiple Choice Prüfung aus der Datei /cld/exm/mpc/date/JahrMonatTag/JahrMonatTag.pdf in LaTeX ab und stelle die Datei in /cld/exm/mpc/date/JahrMonatTag/Fragenummer/Fragenummer.tex hinein.

## Lösung Abtippen (3 Punkte)
Such die eine beliebige abgetippte und handschriftlich gelöste Frage/Angabe aus. Tippe die Lösung in das LaTeX File (ohne ein Grafik zu erstellen) und kopiere den gesammten Inhalt des Ordners worin sich die Frage befindet in den closed Bereich. Also z.B. von /opn/exm/mdl/date/JahrMonatTag/Fragenummer/ nach /cld/exm/mdl/date/JahrMonatTag/Fragenummer/. Lösch den Inhalt des Beispielordners im open Bereich und ändere den Ordnernamen auf /Fragenummer-done. Dadurch wird verhindert, dass die Frage erneut gelöst wird und sie in dem erzeugten pdf nicht doppelt vorkommt.

## Grafik erstellen (4 Punkte)
Du bist in der Königsdisziplin angekommen. Erstelle eine Grafik zu einem beliebigen Beispiel mit TikZ in LaTeX.

## Vorlesung filmen
Vergewissere dich, dass du dazu die Genehmigung von dem Professor hast. Filme die Vorlesungen mit einer Kamera, am besten mit Stativ und stelle sie in hlp/vid/date/ hinein. 

