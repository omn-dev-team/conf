# PDFs generieren mit Windows Subsystem for Linux (WSL)

WSL ist eine Möglichkeit, Linuxprogramme und bash-Scripts direkt auf Windows auszuführen, ohne selbst eine Virtual Machine (VM) aufsetzen zu müssen.

**Vorraussetzungen:**

* mind. Windows 10 build 16237 (überprüfbar mit `WIN+R`, Eingabe von `winver` und `Enter` drücken)
* mind. 3 GB Speicherplatz

## WSL installieren

1. Als ersten Schritt muss [WSL installiert werden](https://docs.microsoft.com/de-de/windows/wsl/install-win10). Ebenso muss eine Linux-Distribution gewählt werden. Hier im Tutorial arbeiten wir mit Ubuntu 20.04 LTS.  
2. Nach dem Installieren von Ubuntu über den Microsoft Store starten wir Ubuntu (z.B. über das Startmenü) und erstellen unseren Linux-User. Welchen Namen und welches Passwort ihr hier wählt ist egal, solange ihr es euch merkt oder wo abspeichert.

*(Beim Eintippen des Passworts wird das tippen selbst nicht angezeigt, das ist in der Linux-Welt normal.)*

![](installation1.png)

Nach dem Erstellen des Users sehen wir den Welcome-Screen von Ubuntu mit ein paar Systeminformationen.

3. Nachdem wir gerade frisch Ubuntu installiert haben sollten wir es aktualisieren. Gib `sudo apt update` ein und drücke `Enter`. Nach dem Eintippen des vorher gesetzten Passwords werden die Paketquellen aktualisiert.  
1. Anschließend werden mit `sudo apt upgrade` die Pakete aktualisiert. 

![](installation2.png)

Ubuntu ist nun installiert und aktuell.

## TeX Live installieren

5. Als nächsten Schritt brauchen wir das [install-tl-Script](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz) für TeX Live.
Dieses speichern wir im Ubuntu-home-Verzeichnis ab. Dieses findet man am Schnellsten, wenn man in die Adresszeile des Explorers `\\wsl$` eingibt und `Enter` drückt.

![](save_location.png)

6. Navigiere nun zu `Ubuntu-20.04\home\Username`, wobei Username dein gewählter Name von vorher ist. Speichere dort die **install-tl-unx.tar.gz** ab.

1. Zurück im Ubuntu-Terminal überprüfen wir mit `ls` ob die Datei aufscheint. Falls ja, entpacken wir die Datei mit `tar xvzf install-tl-unx.tar.gz`

![](untar.png)

8. Mit `cd install-tl-20200822/` (ev. ist die Zahl bei euch anders) wechseln wir in das entpackte Verzeichnis. Nun führen wir mit `sudo ./install-tl` und `Enter` das Installationsskript aus und sollten einen Installationsdialog sehen.

1. Mit `O` und `Enter`, `L` und `Enter` und anschließend drei Mal `Enter` setzen wir die Symlinks, damit latex zum PATH hinzugefügt und später einfach aufgerufen werden kann. Bei den Symlinks sollte nun Folgendes stehen:

```
binaries to: /usr/local/bin
manpages to: /usr/local/man
    info to: /usr/local/info
```
Mit `R` und `Enter` zurück zum Hauptmenü.

 10. *(Optional)* Wenn man mehrere GB Speicherplatz sparen und die Installation beschleunigen möchte, kann man nun mit `C` und `Enter` zu einem Menü, in dem man Paket-Kollektionen abwählen kann (und muss später eventuell einzelne Pakete nachinstallieren). Meine mindestens empfohlenen Kollektionen sind folgende:  

![](texlive_options.png)

11. Nach dem Aus- bzw. Abwählen gelangt man mit `R` und `Enter` zurück zum Installationsmenü.

1. Mit `I` startet man nun die Installation. Diese kann einige Zeit dauern. In meinem Fall waren es ~40 min.

1. Nach Abschluss der Installation sollte man mit `latex -v` überprüfen ob latex erfolgreich installiert wurde und der Befehl erkannt wird. Der Output sollte so ausschauen:

```
pdfTeX 3.14159265-2.6-1.40.21 (TeX Live 2020)
kpathsea version 6.3.2
Copyright 2020 Han The Thanh (pdfTeX) et al.
There is NO warranty.  Redistribution of this software is
covered by the terms of both the pdfTeX copyright and
the Lesser GNU General Public License.
For more information about these matters, see the file
named COPYING and the pdfTeX source.
Primary author of pdfTeX: Han The Thanh (pdfTeX) et al.
Compiled with libpng 1.6.37; using libpng 1.6.37
Compiled with zlib 1.2.11; using zlib 1.2.11
Compiled with xpdf version 4.02
```

Falls eine Meldung kommt, dass der Befehl unbekannt ist ist entweder die Installation fehlgeschlagen oder es wurde vergessen die Symlinks in Schritt 9 zu erstellen. Deinstallieren kann man TeX Live durch `cd /usr/local/ && rm -rf texlive/`.  
Das Löschen kann ein paar Minuten dauern, in dieser Zeit wird in der Shell nichts angezeigt.

## Installieren der restlichen Voraussetzungen

14. Wir installieren das package *make* um makefiles ausführen zu können: `sudo apt install make`
1. *tlmgr* ist der Paketmanager von TeX Live. Dieser wird gebraucht um LaTeX-Packages zu aktualisieren und fehlende zu installieren. Falls ihr die schlankere Packagekonfiguration aus Schritt 10 gewählt habt brauchen wir noch das package *bbm-macros*  
`sudo tlmgr install bbm-macros` installiert die *bbm*-Font-Kollektion

## Kompilieren eines Repositories 

16. Wir sind fertig! Nun kann einfach der <span>README.md</span> des jeweiligen Repositories gefolgt werden um PDFs zu generieren.
1. Wenn wir auf Windows-Ordner zugreifen wollen können wir das in Ubuntu mit `cd /mnt/c/Pfad/zum/Ordner/`, wobei *c* euer Laufwerksbuchstabe ist und */Pfad/zum/Ordner/* der Pfad zum Stammverzeichnis eures Repositories. 

Sollte das Generieren mit einem "Permission denied"-Fehler fehlschlagen, sollte man Ubuntu als \[Windows-\]Administrator ausführen.

Bei Fragen oder Problemen schau in die [Telegram-Gruppe des OMN](https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ).